package proy.mov.upc.app;

import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import proy.mov.upc.app.adapter.DailyRouteAdapter;
import proy.mov.upc.app.database.CustomDbHelper;
import proy.mov.upc.app.ss.dtos;

public class DailyRouteActivity extends BaseActivity {
    private Spinner dateSpinner;
    private HashMap<Integer,Date> spinnerMap;
    private List<dtos.DailyRouteCustomer> routes;
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    private FloatingActionButton btnViewMap;
    private int UserId;

    private Date selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_route);

        setTitle("Rutas Diarias");

        SharedPreferences prefs = getSharedPreferences("Qroma.App", MODE_PRIVATE);
        this.UserId = prefs.getInt("UserId", 0);

        this.btnViewMap = (FloatingActionButton) findViewById(R.id.btnViewMap);

        this.dateSpinner = (Spinner) findViewById(R.id.dateSpinner);
        routes = new ArrayList<>();
        this.FillDropdown();

        recycler = (RecyclerView) findViewById(R.id.dailyRouterRecycler);
        recycler.setHasFixedSize(true);
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        adapter = new DailyRouteAdapter(routes);
        recycler.setAdapter(adapter);

        this.btnViewMap.setOnClickListener(new  View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ViewMap();
            }
        });
    }

    private void ViewMap(){
        Intent newActivity = new Intent(this, RouteMapActivity.class);
        newActivity.putExtra("SelectedDate", this.selectedDate);
        this.startActivity(newActivity);
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DateSelected(adapterView,view, i, l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                NoDateSelected(adapterView);
            }
        });
    }

    private void FillDropdown(){
        CustomDbHelper customDbHelper = new CustomDbHelper(this);
        List<Date> dates = customDbHelper.GetDailyRouteDates(this.UserId);
        String[] dateStrings = new String[dates.size()];
        spinnerMap = new HashMap<>();

        DateFormat iso8601Format = new SimpleDateFormat("dd-MM-yyyy");

        for(int i = 0; i < dates.size(); i++){
            spinnerMap.put(i,dates.get(i));
            dateStrings[i] = iso8601Format.format(dates.get(i));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, dateStrings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.dateSpinner.setAdapter(adapter);
    }

    private void DateSelected(AdapterView<?> adapterView, View view, int i, long l){
        int index = adapterView.getSelectedItemPosition();
        selectedDate = spinnerMap.get(i);
        CustomDbHelper db = new CustomDbHelper(this);
        routes = db.GetDailyRoutesByUserAndDate(this.UserId, selectedDate);
        adapter = new DailyRouteAdapter(routes);
        recycler.setAdapter(adapter);
        this.btnViewMap.setEnabled(true);
    }

    private void NoDateSelected(AdapterView<?> adapterView){
        this.btnViewMap.setEnabled(false);
    }
}
