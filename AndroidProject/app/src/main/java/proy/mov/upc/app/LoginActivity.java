package proy.mov.upc.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.servicestack.android.AndroidServiceClient;
import net.servicestack.client.*;

import proy.mov.upc.app.ss.ClientFactory;
import proy.mov.upc.app.ss.dtos;

public class LoginActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private EditText txtPassword;
    private Button btnLogin;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle("Login");

        this.txtPassword = findViewById(R.id.txtPassword);
        this.txtUsuario = findViewById(R.id.txtUsername);
        this.btnLogin = findViewById(R.id.btnLogin);
        this.progressBar = findViewById(R.id.progressbar);

        progressBar.setVisibility(View.INVISIBLE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
              DoLogin();
            }
        });
    }

    private void DoLogin(){
        String userName = txtUsuario.getText().toString();
        String password = txtPassword.getText().toString();
        dtos.Authenticate request = new dtos.Authenticate()
                                        .setProvider("credentials")
                                        .setUserName(userName)
                                        .setPassword(password);


        progressBar.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);

        AndroidServiceClient  client = ClientFactory.getClient(true);
        client.postAsync(request, new AsyncResult<dtos.AuthenticateResponse>() {
            @Override
            public void success(dtos.AuthenticateResponse response) {
                super.success(response);
                OnLoginSuccess(response);
            }

            @Override
            public void error(Exception ex) {
                super.error(ex);
                OnLoginError(ex);
            }
        });
    }

    private void OnLoginSuccess(dtos.AuthenticateResponse response){
        progressBar.setVisibility(View.INVISIBLE);
        btnLogin.setEnabled(true);

        int userId = Integer.parseInt(response.getUserId());

        SharedPreferences.Editor editor = getSharedPreferences("Qroma.App", MODE_PRIVATE).edit();
        editor.putInt("UserId", userId);
        editor.putString("Token", response.getBearerToken());
        editor.putString("UserName", response.getUserName());
        editor.apply();

        Intent newActivity = new Intent(this, SyncActivity.class);
        this.startActivity(newActivity);
    }

    private void OnLoginError(Exception ex) {
        //net.servicestack.client.WebServiceException

        String message;
        if (ex instanceof net.servicestack.client.WebServiceException) {
            net.servicestack.client.WebServiceException wsex = (net.servicestack.client.WebServiceException) ex;
            message = wsex.getResponseStatus().getMessage();
        }
        else{
            message = "An error has occured";
        }

        progressBar.setVisibility(View.INVISIBLE);
        btnLogin.setEnabled(true);

        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sharedPrefs = getSharedPreferences("Qroma.App", MODE_PRIVATE);
        if(sharedPrefs.contains("Token")){
            Intent newActivity = new Intent(this, SyncActivity.class);
            this.startActivity(newActivity);
        }
    }
}