package proy.mov.upc.app;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.List;

import proy.mov.upc.app.adapter.OrderDetailAdapter;
import proy.mov.upc.app.adapter.ProductSearchAdapter;
import proy.mov.upc.app.database.CustomDbHelper;
import proy.mov.upc.app.ss.dtos;

public class OrderActivity extends BaseActivity {
    private dtos.DailyRouteCustomer routeCustomer;
    private TextView lblClientName;
    private TextView lblClientAdress;
    private FloatingActionButton btnaddProduct;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    private List<Pair<dtos.Product, dtos.OrderDetail>> orderDetails;
    //orderDetailRecycler

    private final int PRODUCT_SEARCH_RESULT_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        setTitle("Pedidos");

        this.lblClientName = (TextView) findViewById(R.id.lblOrder_ClientName);
        this.lblClientAdress = (TextView) findViewById(R.id.lblOrder_ClientAdress);
        loadDailyRouteCustomer();

        this.btnaddProduct = (FloatingActionButton) findViewById(R.id.btnOrder_addProduct);

        this.btnaddProduct.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), ProductSearchActivity.class);
                startActivityForResult(i, PRODUCT_SEARCH_RESULT_CODE);
            }
        });

        this.LoadOrderDetails(this.routeCustomer.getId());

        this.recycler = (RecyclerView) findViewById(R.id.orderDetailRecycler);
        this.recycler.setHasFixedSize(true);
        this.lManager = new LinearLayoutManager(this);
        this.recycler.setLayoutManager(lManager);

        this.adapter = new OrderDetailAdapter(this.orderDetails);
        this.recycler.setAdapter(adapter);
    }

    private void LoadOrderDetails(int dailyRouteId){
        CustomDbHelper db = new CustomDbHelper(this);
        this.orderDetails = db.GetOrderDetails(this.routeCustomer.getId());
    }


    private void loadDailyRouteCustomer(){
        Integer dailyRouteId = getIntent().getIntExtra("DailyRouteId",0);

        CustomDbHelper db = new CustomDbHelper(this);
        this.routeCustomer = db.GetDailyRouteCustomer(dailyRouteId);

        this.lblClientName.setText(routeCustomer.getCustomerName());
        this.lblClientAdress.setText(routeCustomer.getCustomerAddress());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (PRODUCT_SEARCH_RESULT_CODE) : {
                if (resultCode == Activity.RESULT_OK) {
                    // TODO Extract the data returned from the child Activity.
                    int returnValue = data.getIntExtra("ProductId",0);
                    if(returnValue != 0){
                        AddProductToOrder(returnValue);
                    }
                }
                break;
            }
        }
    }

    private void AddProductToOrder(int productId) {
        CustomDbHelper db = new CustomDbHelper(this);
        dtos.OrderDetail od = new dtos.OrderDetail();
        od.setDailyRouteId(this.routeCustomer.getId());
        od.setAmount(new BigDecimal(1));
        od.setProductId(productId);
        db.AddProductToOrder(od);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
