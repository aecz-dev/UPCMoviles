package proy.mov.upc.app;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.math.BigDecimal;

import proy.mov.upc.app.database.CustomDbHelper;
import proy.mov.upc.app.ss.dtos;

public class ProductOrderEditActivity extends BaseActivity {
    private TextView lblProductName;
    private TextView lblUnitOfMeasurement;
    private TextView lblPrice;
    private EditText txtAmount;
    private FloatingActionButton btnDelete;
    private FloatingActionButton btnSave;
    private int OrderDetailId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_order_edit);

        setTitle("Editar Orden");

        this.lblProductName = (TextView) findViewById(R.id.lblOrderEdit_ProductName);
        this.lblUnitOfMeasurement = (TextView) findViewById(R.id.lblOrderEdit_UnitOfMeasurement);
        this.lblPrice = (TextView) findViewById(R.id.lblOrderEdit_Price);
        this.txtAmount = (EditText) findViewById(R.id.txtOrderEdit_Amount);

        this.btnDelete = (FloatingActionButton) findViewById(R.id.btnOrderEdit_Delete);
        this.btnSave = (FloatingActionButton) findViewById(R.id.btnOrderEdit_Save);

        this.OrderDetailId = getIntent().getIntExtra("OrderDetailId",0);

        CustomDbHelper db = new CustomDbHelper(this);
        Pair<dtos.Product, dtos.OrderDetail> od = db.GetOrderDetailById(this.OrderDetailId);

        this.lblProductName.setText(od.first.getName());
        this.lblUnitOfMeasurement.setText(od.first.getUnitOfMeasurement());
        this.lblPrice.setText(od.first.getPrice().toString());
        this.txtAmount.setText(od.second.getAmount().toString());

        this.btnDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DoDelete();
            }
        });

        this.btnSave.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DoUpdate();
            }
        });
    }

    private void DoDelete(){
        CustomDbHelper db = new CustomDbHelper(this);
        db.DeleteProductOrder(this.OrderDetailId);
        finish();
    }

    private void DoUpdate(){


        if(this.txtAmount.getText().toString().matches("\\d+(?:\\.\\d+)?")) {
            CustomDbHelper db = new CustomDbHelper(this);
            db.UpdateProductOrder(this.OrderDetailId, new BigDecimal(this.txtAmount.getText().toString()));
            finish();
        }
        else
        {
            Toast toast = Toast.makeText(this, "Ingrese un número decimal.", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
