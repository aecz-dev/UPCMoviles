package proy.mov.upc.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import java.util.List;

import proy.mov.upc.app.adapter.DailyRouteAdapter;
import proy.mov.upc.app.adapter.ProductSearchAdapter;
import proy.mov.upc.app.database.CustomDbHelper;
import proy.mov.upc.app.ss.dtos;

public class ProductSearchActivity extends BaseActivity {
    private EditText txtFilter;
    private List<dtos.Product> products;
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        setTitle("Buscar Producto");

        LoadProducts();

        this.recycler = (RecyclerView) findViewById(R.id.productsRecycler);
        this.recycler.setHasFixedSize(true);
        this.lManager = new LinearLayoutManager(this);
        this.recycler.setLayoutManager(lManager);

        this.adapter = new ProductSearchAdapter(this.products, this);
        this.recycler.setAdapter(adapter);
    }

    private void LoadProducts(){
        CustomDbHelper customDbHelper = new CustomDbHelper(this);
        this.products = customDbHelper.GetProducts();
    }
}
