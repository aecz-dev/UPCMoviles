package proy.mov.upc.app;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v4.app.Fragment;

import java.util.Date;
import java.util.List;

import proy.mov.upc.app.database.CustomDbHelper;
import proy.mov.upc.app.ss.dtos;

public class RouteMapActivity extends FragmentActivity implements OnMapReadyCallback{
    private GoogleMap mMap;
    private List<dtos.DailyRouteCustomer> routes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_map);


        Date selectedDate = (Date) getIntent().getSerializableExtra("SelectedDate");

        SharedPreferences prefs = getSharedPreferences("Qroma.App", MODE_PRIVATE);
        int userId = prefs.getInt("UserId", 0);

        CustomDbHelper db = new CustomDbHelper(this);
         routes = db.GetDailyRoutesByUserAndDate(userId, selectedDate);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng loc = null;
        UiSettings mapUtils = mMap.getUiSettings();
        mapUtils.setZoomControlsEnabled(true);

        for(dtos.DailyRouteCustomer route : routes){
            loc = new LatLng(route.getCustomerLatitude().doubleValue(), route.getCustomerLongitude().doubleValue());
            mMap.addMarker(new MarkerOptions().position(loc).title(route.customerName));
        }
        if(loc != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 16));
        }


    }
}
