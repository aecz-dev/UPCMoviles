package proy.mov.upc.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import proy.mov.upc.app.OrderActivity;
import proy.mov.upc.app.R;
import proy.mov.upc.app.ss.dtos;

/**
 * Created by Alvaro on 16/02/2018.
 */

public class ProductSearchAdapter extends RecyclerView.Adapter<ProductSearchAdapter.ProductSearchHolder> {
    private List<dtos.Product> items;
    private Activity aActivity;

    public static class ProductSearchHolder extends RecyclerView.ViewHolder {

        private CardView productSearch_card;
        private TextView lblName;
        private TextView lblUnitOfMeasurement;
        private TextView lblPrice;
        private int productId;
        private Activity hActivity;

        public ProductSearchHolder(View v, Activity activity) {
            super(v);
            this.hActivity= activity;
            productSearch_card = (CardView) v.findViewById(R.id.productSearch_card);
            lblName = (TextView) v.findViewById(R.id.lblProductSearchCard_name);
            lblUnitOfMeasurement = (TextView) v.findViewById(R.id.lblProductSearchCard_unitOfMeasurement);
            lblPrice = (TextView) v.findViewById(R.id.lblProductSearchCard_price);


            productSearch_card.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("ProductId",productId);
                    hActivity.setResult(Activity.RESULT_OK,returnIntent);
                    hActivity.finish();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public ProductSearchAdapter(List<dtos.Product> items, Activity aActivity){
        this.items = items;
        this.aActivity = aActivity;
    }

    public List<dtos.Product> getItems() {return this.items;}

    @Override
    public ProductSearchAdapter.ProductSearchHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.productsearch_card, viewGroup, false);
        return new ProductSearchAdapter.ProductSearchHolder(v,this.aActivity);
    }

    @Override
    public void onBindViewHolder(ProductSearchAdapter.ProductSearchHolder holder, int position) {
        holder.lblName.setText(items.get(position).getName());
        holder.lblUnitOfMeasurement.setText(items.get(position).getUnitOfMeasurement());
        holder.lblPrice.setText(items.get(position).getPrice().toString());
        holder.productId = items.get(position).getId();
    }
}
