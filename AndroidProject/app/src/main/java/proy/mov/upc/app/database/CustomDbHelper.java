package proy.mov.upc.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Pair;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import proy.mov.upc.app.ss.dtos;

/**
 * Created by Alvaro on 08/02/2018.
 */

public class CustomDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "AppDb.db";

    public CustomDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS Product ("
                +   "Id INTEGER PRIMARY KEY ,"
                + "Name VARCHAR(120),"
                + "UnitOfMeasurement VARCHAR(8),"
                + "Price INTEGER)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS DailyRouteCustomer ("
                +   "Id INTEGER PRIMARY KEY ,"
                + "Date VARCHAR(200),"
                + "CustomerId INTEGER,"
                + "CustomerName VARCHAR(200),"
                + "CustomerRUC VARCHAR(200),"
                + "CustomerLatitude INTEGER,"
                + "CustomerLongitude INTEGER,"
                + "CustomerAddress VARCHAR(300),"
                + "UserId INTEGER)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS OrderDetail ("
                +  "Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "ProductId INTEGER,"
                + "DailyRouteId INTEGER,"
                + "Amount DECIMAL(18,6))"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public List<dtos.Product> GetProducts(){
        List<dtos.Product> products = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query("Product", null, null, null, null, null, null);
        while(c.moveToNext()){
            dtos.Product item = new dtos.Product();
            item.setId(c.getInt(0));
            item.setName(c.getString(1));
            item.setUnitOfMeasurement(c.getString(2));
            BigDecimal price = BigDecimal.valueOf(c.getInt(3)).movePointLeft(2);
            item.setPrice(price);
            products.add(item);
        }

        return products;
    }

    public dtos.Product GetProduct(int id){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query("Product", null, "Id=?", new String[]{ String.valueOf(id) }, null, null, null);
        if(c.moveToNext()){
            dtos.Product item = new dtos.Product();
            item.setId(c.getInt(0));
            item.setName(c.getString(1));
            item.setUnitOfMeasurement(c.getString(2));
            BigDecimal price = BigDecimal.valueOf(c.getInt(3)).movePointLeft(2);
            item.setPrice(price);
            return item;
        }
        else{
            return null;
        }
    }

    public void ReplaceProducts(List<dtos.Product> products){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM Product");

        for(dtos.Product p : products){
            db.insert("Product", null, this.ProductToCV(p));
        }
    }

    private ContentValues ProductToCV(dtos.Product product){
        ContentValues values = new ContentValues();
        values.put("Id", product.getId());
        values.put("Name", product.getName());
        values.put("UnitOfMeasurement", product.getUnitOfMeasurement());
        values.put("Price", product.getPrice().movePointRight(2).intValue());
        return values;
    }

    public void ReplaceDailyRoutes(List<dtos.DailyRouteCustomer> routes){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM DailyRouteCustomer");

        for(dtos.DailyRouteCustomer r : routes){
            db.insert("DailyRouteCustomer", null, this.DailyRouteToCV(r));
        }
    }

    private ContentValues DailyRouteToCV(dtos.DailyRouteCustomer dailyRoute){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        ContentValues values = new ContentValues();
        values.put("Id", dailyRoute.getId());
        values.put("Date", sdf.format(dailyRoute.getDate()));
        values.put("CustomerId", dailyRoute.getCustomerId());
        values.put("CustomerName", dailyRoute.getCustomerName());
        values.put("CustomerRUC", dailyRoute.getCustomerRUC());;
        values.put("CustomerLatitude", dailyRoute.getCustomerLatitude().movePointRight(6).intValue());
        values.put("CustomerLongitude", dailyRoute.getCustomerLongitude().movePointRight(6).intValue());
        values.put("CustomerAddress", dailyRoute.getCustomerAddress());
        values.put("UserId", dailyRoute.getUserId());

        return values;
    }

    public List<Date> GetDailyRouteDates(int userId){
        List<Date> dates = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(
                "DailyRouteCustomer",
                new String[]{"Date"},
                "UserId=?",
                new String[] { String.valueOf(userId) },
                "Date",
                null,
                null);
        while(c.moveToNext()){
            Date date;
            String dateTime = c.getString(0);
            DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                date = iso8601Format.parse(dateTime);
            } catch (ParseException e) {
                date = new Date();
            }
            dates.add(date);
        }

        return dates;
    }

    public List<dtos.DailyRouteCustomer> GetDailyRoutesByUserAndDate(int userId, Date searchDate){
        List<dtos.DailyRouteCustomer> drcs = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String searchDateString = sdf.format(searchDate);

        Cursor c = db.query("DailyRouteCustomer", null, "UserId=? AND Date=?", new String[] { String.valueOf(userId), searchDateString }, null, null, null);
        while(c.moveToNext()) {
            dtos.DailyRouteCustomer item = new dtos.DailyRouteCustomer();
            item.setId(c.getInt(0));
            String dateTime = c.getString(1);
            Date date;
            DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                date = iso8601Format.parse(dateTime);
            } catch (ParseException e) {
                date = new Date();
            }
            item.setDate(date);
            item.setCustomerId(c.getInt(2));
            item.setCustomerName(c.getString(3));
            item.setCustomerRUC(c.getString(4));
            BigDecimal latitude = BigDecimal.valueOf(c.getInt(5)).movePointLeft(6);
            item.setCustomerLatitude(latitude);
            BigDecimal longitude = BigDecimal.valueOf(c.getInt(6)).movePointLeft(6);
            item.setCustomerLongitude(longitude);
            item.setCustomerAddress(c.getString(7));
            item.setUserId(c.getInt(8));
            drcs.add(item);
        }

        return drcs;
    }

    public dtos.DailyRouteCustomer GetDailyRouteCustomer(int id){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query("DailyRouteCustomer", null, "Id=?", new String[] { String.valueOf(id) }, null, null, null);
        if(c.moveToNext()){
            dtos.DailyRouteCustomer item = new dtos.DailyRouteCustomer();
            item.setId(c.getInt(0));
            String dateTime = c.getString(1);
            Date date;
            DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                date = iso8601Format.parse(dateTime);
            } catch (ParseException e) {
                date = new Date();
            }
            item.setDate(date);
            item.setCustomerId(c.getInt(2));
            item.setCustomerName(c.getString(3));
            item.setCustomerRUC(c.getString(4));
            BigDecimal latitude = BigDecimal.valueOf(c.getInt(5)).movePointLeft(6);
            item.setCustomerLatitude(latitude);
            BigDecimal longitude = BigDecimal.valueOf(c.getInt(6)).movePointLeft(6);
            item.setCustomerLongitude(longitude);
            item.setCustomerAddress(c.getString(7));
            item.setUserId(c.getInt(8));

            return item;
        }
        else{
            return null;
        }

    }

    public void AddProductToOrder(dtos.OrderDetail detail){
        SQLiteDatabase db = getWritableDatabase();
        db.insert("OrderDetail", null, this.OrderDetailToCV(detail));
    }

    public void UpdateProductOrder(int orderDetailId, BigDecimal amount){
        SQLiteDatabase db = getWritableDatabase();
        int amoutInt = amount.movePointRight(2).intValue();

        db.execSQL("UPDATE OrderDetail SET Amount=? WHERE Id=?", new String[]{String.valueOf(amoutInt), String.valueOf(orderDetailId)});
    }

    public void DeleteProductOrder(int orderDetailId){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM OrderDetail WHERE Id=?", new String[]{String.valueOf(orderDetailId)});
    }

    private ContentValues OrderDetailToCV(dtos.OrderDetail orderDetail){
        ContentValues values = new ContentValues();
        values.put("Id", orderDetail.getId());
        values.put("ProductId", orderDetail.getProductId());
        values.put("DailyRouteId", orderDetail.getDailyRouteId());
        values.put("Amount", orderDetail.getAmount().movePointRight(2).intValue());
        return values;
    }

    private String ORDER_DETAIL_QUERY = "SELECT OD.*, P.Id as ProductId, P.Name as ProductName, P.UnitOfMeasurement as ProductUOM, P.Price as ProductPrice "
    + "FROM OrderDetail as OD INNER JOIN Product as P ON P.Id = OD.ProductId  "
    + "WHERE OD.DailyRouteId=?";

    public List<Pair<dtos.Product, dtos.OrderDetail>> GetOrderDetails(int routeId) {
        List<Pair<dtos.Product, dtos.OrderDetail>> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(ORDER_DETAIL_QUERY, new String[]{String.valueOf(routeId)});
        while (c.moveToNext()) {
            dtos.OrderDetail od = new dtos.OrderDetail();
            od.setId(c.getInt(0));
            od.setProductId(c.getInt(1));
            od.setDailyRouteId(c.getInt(2));
            BigDecimal amount = BigDecimal.valueOf(c.getInt(3)).movePointLeft(2);
            od.setAmount(amount);

            dtos.Product product = new dtos.Product();
            product.setId(c.getInt(4));
            product.setName(c.getString(5));
            product.setUnitOfMeasurement(c.getString(6));
            BigDecimal price = BigDecimal.valueOf(c.getInt(7)).movePointLeft(2);
            product.setPrice(price);
            Pair<dtos.Product, dtos.OrderDetail> pair = new Pair<>(product, od);
            list.add(pair);
        }

        return list;
    }

    private String ORDER_DETAIL_BYUSERS_QUERY = "SELECT OD.* "
            + "FROM OrderDetail as OD INNER JOIN DailyRouteCustomer as DRC ON OD.DailyRouteId = DRC.Id "
            + "WHERE DRC.UserId=?";

    public ArrayList<dtos.OrderDetail> GetOrderDetailsByUser(int id){
        ArrayList<dtos.OrderDetail> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(ORDER_DETAIL_BYUSERS_QUERY, new String[]{String.valueOf(id)});
        while (c.moveToNext()) {
            dtos.OrderDetail od = new dtos.OrderDetail();
            od.setId(c.getInt(0));
            od.setProductId(c.getInt(1));
            od.setDailyRouteId(c.getInt(2));
            BigDecimal amount = BigDecimal.valueOf(c.getInt(3)).movePointLeft(2);
            od.setAmount(amount);

            list.add(od);
        }

        return list;
    }

    private String ORDER_DETAIL_SINGLE_QUERY = "SELECT OD.*, P.Id as ProductId, P.Name as ProductName, P.UnitOfMeasurement as ProductUOM, P.Price as ProductPrice "
            + "FROM OrderDetail as OD INNER JOIN Product as P ON P.Id = OD.ProductId  "
            + "WHERE OD.Id=?";

    public Pair<dtos.Product, dtos.OrderDetail> GetOrderDetailById(int orderDetailId) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(ORDER_DETAIL_SINGLE_QUERY, new String[]{String.valueOf(orderDetailId)});
        if (c.moveToNext()) {
            dtos.OrderDetail od = new dtos.OrderDetail();
            od.setId(c.getInt(0));
            od.setProductId(c.getInt(1));
            od.setDailyRouteId(c.getInt(2));
            BigDecimal amount = BigDecimal.valueOf(c.getInt(3)).movePointLeft(2);
            od.setAmount(amount);

            dtos.Product product = new dtos.Product();
            product.setId(c.getInt(4));
            product.setName(c.getString(5));
            product.setUnitOfMeasurement(c.getString(6));
            BigDecimal price = BigDecimal.valueOf(c.getInt(7)).movePointLeft(2);
            product.setPrice(price);
            Pair<dtos.Product, dtos.OrderDetail> pair = new Pair<>(product, od);
            return pair;
        }
        else{
            return null;
        }
    }
}