'use strict'
const path = require('path')

module.exports = {
    build: {
        jsRoot: path.resolve(__dirname, '../../2.BackEnd/UPC.Mov.WebSite/wwwroot/js/'),
    },
    local: {
        env: 'local',
        browserSyncOptions: {
            host: 'localhost',
            port: 3001,
            proxy: 'http://localhost:53909/',
        }
    }
}