import layout from './layout.vue'
import layout_content from './layout-content.vue'
import layout_drawer from './layout-drawer.vue'
import layout_footer from './layout-footer.vue'
import layout_toolbar from './layout-toolbar.vue'


var layoutComponents = {
    layout,
    layout_content,
    layout_drawer,
    layout_footer,
    layout_toolbar
}

export { layoutComponents }