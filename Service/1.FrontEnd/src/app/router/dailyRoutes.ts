import { dailyRouteComponents } from '../modules/daily-route'
import { RouteConfig } from 'vue-router'

var dailyRoutes: Array<RouteConfig> = [

    <RouteConfig>{
        path: '/daily-routes',
        name: 'daily-routes',
        component: dailyRouteComponents.daily_route
    }
]

export { dailyRoutes }