import Vue from 'vue'
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import layout from './modules/_layout/layout.vue'
import { router } from './router'
import { components } from './modules'

Vue.use(Vuetify);
Vue.use(VueRouter);

for (var k in components) {
    Vue.component(components[k].extendOptions.name, components[k]);
}

var vueInstance = new Vue({
    router,
    render: createElement => createElement(layout)
})

export { vueInstance }