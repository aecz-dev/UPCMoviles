import 'shared/polyfills'
import 'shared/styles'
import { vueInstance } from './vue-init'
import { ServiceFactory, dtos } from 'shared/service'

var client = ServiceFactory.createClient(false);
var request = new dtos.Authenticate();

client.get(request)
    .then(() => {
        window.location.replace("/app/main/")
    })
    .catch(() => {
        vueInstance.$mount('#vue-app')
    })