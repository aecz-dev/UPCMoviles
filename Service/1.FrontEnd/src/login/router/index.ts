import VueRouter from "vue-router";
import { routes } from './routes'

var router = new VueRouter({
    routes
})

export { router }