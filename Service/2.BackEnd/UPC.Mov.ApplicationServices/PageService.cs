﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using ServiceStack;
using UPC.Mov.Core.Dtos;

namespace UPC.Mov.ApplicationServices
{
    public class PageService : Service
    {
        private IHostingEnvironment _hostingEnviroment;

        public PageService(IHostingEnvironment hostingEnvironment)
        {
            this._hostingEnviroment = hostingEnvironment;
        }

        private string GetPage(string page)
        {
            string webRootPath = this._hostingEnviroment.WebRootPath;
            var file = System.IO.Path.Combine(webRootPath, page);
            return System.IO.File.ReadAllText(file);
        }

        public string Get(AppPageRequest request)
        {
            if (request.Page == "login")
            {
                return GetPage("login.html");
            }
            else if (request.Page == "main")
            {
                return GetPage("app.html");
            }
            else
            {
                throw HttpError.NotFound("Page not found");
            }
        }
    }
}