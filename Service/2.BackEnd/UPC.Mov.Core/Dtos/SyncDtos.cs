﻿using ServiceStack;
using System;
using System.Collections.Generic;
using UPC.Mov.Core.Mapping.Tables;
using UPC.Mov.Core.Mapping.Views;

namespace UPC.Mov.Core.Dtos
{
    [Route("/Sync/Product", Verbs = "GET")]
    public class GetProductList : IGet, IReturn<IEnumerable<Product>>
    {        
    }
    
    [Route("/Sync/Route", Verbs = "GET")]
    public class GetRouteList : IGet, IReturn<IEnumerable<DailyRouteCustomer>>
    {        
    }

    [Route("/Sync/Order", Verbs ="POST")]
    public class ReplaceOrder : IPost, IReturn<IEnumerable<string>>
    {        
        public IEnumerable<OrderDetail> OrderDetails { get; set; }
    }


    [Route("/Sync/Order", Verbs = "GET")]
    public class GetOrderDetails : IGet, IReturn<IEnumerable<OrderDetail>>
    {        
    }
    
}