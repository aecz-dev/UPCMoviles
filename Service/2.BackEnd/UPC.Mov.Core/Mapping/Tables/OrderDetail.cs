﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace UPC.Mov.Core.Mapping.Tables
{
    public class OrderDetail
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public int DailyRouteId { get; set; }
        public int ProductId { get; set; }
        public decimal Amount { get; set; }
    }
}
