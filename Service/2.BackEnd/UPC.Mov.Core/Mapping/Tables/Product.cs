﻿using ServiceStack.DataAnnotations;

namespace UPC.Mov.Core.Mapping.Tables
{
    public class Product
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string UnitOfMeasurement { get; set; }

        public decimal Price { get; set; }
    }
}