﻿CREATE TABLE [dbo].[ConfigSetting] (
    [Id]    VARCHAR (8000) NOT NULL,
    [Value] VARCHAR (8000) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

