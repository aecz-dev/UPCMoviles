﻿CREATE VIEW [dbo].[DailyRouteCustomer]
AS 
SELECT DR.Id, 
		DR.Date, 
		DR.CustomerId, 
		C.RUC as 'CustomerRUC',
		C.Name as 'CustomerName',
		C.Latitude as 'CustomerLatitude',
		C.Longitude as 'CustomerLongitude',
		C.Address as 'CustomerAddress',
		DR.UserId
FROM DailyRoute DR INNER JOIN Customer C
		ON DR.CustomerId  = C.Id